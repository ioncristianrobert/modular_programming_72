<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\GeneratedValue
     * @ORM\Column(type="text")
     */
    private $firstname;
    /**
     * @ORM\GeneratedValue
     * @ORM\Column(type="text")
     */
    private $lastname;
    /**
     * @ORM\GeneratedValue
     * @ORM\Column(type="text")
     */
    private $email;

    // add your own fields
}
